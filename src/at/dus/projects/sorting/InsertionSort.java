package at.dus.projects.sorting;

public class InsertionSort implements Sorter{
    @Override
    public int[] sort(int[] arr2) {
        for(int j = 1; j < arr2.length; j++){
            int key = arr2[j];
            int i =j-1;
            while ((i>-1) && (arr2[i] > key)){
                arr2 [i+1] = arr2[i];
                i--;
            }
            arr2[i+1] = key;
        }
        return arr2;
    }
}
