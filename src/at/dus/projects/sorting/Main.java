package at.dus.projects.sorting;

public class Main {
    public static void main(String[] args) {
        int [] arr = SortHelper.generateArray(10);
        Sorter sorter = new BubbleSort();
        System.out.println("Array before Bubble Sort");
        printArray(arr);
        int[] sorted = sorter.sort(arr);
        System.out.println("Array after Bubble Sort");
        printArray(sorted);

        int [] arr2 = SortHelper.generateArray(10);
        Sorter insertion = new InsertionSort();
        System.out.println("Array before Insertion Sort");
        printArray(arr2);
        int [] sorted2 = insertion.sort(arr2);
        System.out.println("Array after Insertion Sort");
        printArray(sorted2);

        int [] arr3 = SortHelper.generateArray(10);
        Sorter selection = new SelectionSort();
        System.out.println("Array before Selection Sort");
        printArray(arr3);
        int [] sorted3 = selection.sort(arr3);
        System.out.println("Array after Selection Sort");
        printArray(sorted3);

        int n = arr.length;
        int[][] arr4 = new int[10][n];
        Sorter bucket = new BucketSort();
        System.out.println("Array before Selection Sort");
        printArray(arr4[n]);
        int[] sorted4 = bucket.sort(arr4[n]);
        System.out.println("Array after Selection Sort");
        printArray(sorted4);

    }



    private static void printArray(int[] arr){
        for(int value: arr){
            System.out.print(value + " - ");
        }
        System.out.println();
    }

    private static void printArray2d(int[][] arr){
        for (int[] value: arr){
            System.out.print(value + " - ");
        }
        System.out.println();
    }


}
