package at.dus.projects.sorting;

public class SelectionSort implements Sorter{
    @Override
    public int[] sort(int[] arr3) {
        for(int i = 0; i < arr3.length - 1; i++){
            int position = i;
            for (int j= i+1; j < arr3.length; j++){
                if (arr3[j] < arr3[position]){
                    position = j;
                }
            }
            int smallestnumber = arr3[position];
            arr3[position] = arr3[i];
            arr3[i] = smallestnumber;
        }
        return arr3;
    }
}
