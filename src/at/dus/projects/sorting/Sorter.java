package at.dus.projects.sorting;

public interface Sorter {
    public int[] sort(int[] arr);
}
