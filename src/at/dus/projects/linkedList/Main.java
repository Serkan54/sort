package at.dus.projects.linkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();

        list.push(7);
        list.push(6);
        list.push(5);
        list.push(2);
        list.push(4);

        System.out.println("\nCreated Linked List is: ");
        list.printList();

        list.deleteNode(4);

        System.out.println("\nLinked List afer Removing: ");
        list.printList();

        System.out.println("\nLinked List afer Deletion: ");
        list.clear();
        list.printList();
    }
}
