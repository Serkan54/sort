package at.dus.projects.StackList;

public class Main {
    public static void main(String[] args) {
        DUMStack stack = new DUMStack();
        stack.push(17);
        stack.push(18);
        stack.push(19);
        try {
            System.out.println(stack.pop());
            System.out.println(stack.pop());
            System.out.println(stack.pop());
        } catch (StackEmptyException e) {
            System.out.println(e.getMessage());
        }
    }
}
