package at.dus.projects.StackList;

public class DUMList {
    private Node root = null;

    public void addValue(int number){
        Node node = new Node(number);
        if (root==null){
            root = node;
        } else{
            Node n = getLastNode();
            n.setNext(node);
        }
    }

    private Node getLastNode(){
        if(root==null){
            return null;
        }
        Node n = root;
        while(n.getNext()!=null){
            n = n.getNext();
        }
        return n;
    }

    public int get(int index){
        Node n = this.root;
        int cnt = 0;
        boolean found = false;
        while (!found){
            if (index==cnt){
                break;
            }
            n = n.getNext();
            cnt++;
        }
        return n.getNumber();
    }

    public void remove(int index){

    }

    public void printList(){
        Node n = this.root;
        while (n.getNext()!=null){
            System.out.println("value:" + n.getNumber());
            n = n.getNext();
        }
        System.out.println("value:" + n.getNumber());
    }
}
