package at.dus.projects.StackList;

public class DUMStack {
    private Node top;

    public void push(int value){
        Node newNode = new Node(value);
        if(top == null){
            this.top = newNode;
        }else{
            newNode.setNext(this.top);
            this.top = newNode;
        }
    }

    public int pop() throws StackEmptyException{
        if(this.top==null){
            throw new StackEmptyException("Der Stack hat keine Daten");
        }
        Node oldNode = this.top;
        this.top = oldNode.getNext();

        return oldNode.getNumber();


    }
}
