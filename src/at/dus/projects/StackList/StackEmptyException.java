package at.dus.projects.StackList;

public class StackEmptyException extends Exception{
    public StackEmptyException(String message){
        super(message);
    }
}
